package com.example.andrei.lab2loancalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText loanText;
    EditText yearsText;
    EditText interestText;
    TextView monthlyPaymentText;
    TextView totalPaymentText;
    TextView totalInterestText;
    double loan, years, interest, monthlyPayment, totalPayment, totalInterest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getting the necessary fields
        loanText = ((EditText) findViewById(R.id.loan));
        yearsText = (EditText) findViewById(R.id.time);
        interestText = (EditText) findViewById(R.id.interest);
        monthlyPaymentText = (TextView) findViewById(R.id.monthlyPayment);
        totalPaymentText = (TextView) findViewById(R.id.totalPayment);
        totalInterestText = (TextView) findViewById(R.id.totalInterest);

    }

    public void CalculateInterestRate(View v)
    {

        try{
            loan = Double.parseDouble(loanText.getText().toString());
            years = Double.parseDouble(yearsText.getText().toString());
            interest = Double.parseDouble(interestText.getText().toString()) /100.;
        } catch (NumberFormatException e)
        {
            Log.i("NumberFormat", e.getMessage());
            wrongCredentials();
            return;
        } catch(Exception e)
        {
            Log.i("UnknownException", e.getMessage());
            wrongCredentials();
            return;
        }

        double months = years * 12;
        //periodic interest rate
        double i =  interest / 12;
        double dFactor = ((Math.pow( (1 + i), months ) - 1)) / (i * Math.pow((1 + i), months));
        monthlyPayment = loan / dFactor;
        totalPayment = monthlyPayment * months;
        totalInterest = totalPayment - loan;
        monthlyPayment = round(monthlyPayment);
        totalPayment = round(totalPayment);
        totalInterest = round(totalInterest);

        monthlyPaymentText.setText(Double.toString(monthlyPayment));
        totalPaymentText.setText(Double.toString(totalPayment));
        totalInterestText.setText(Double.toString(totalInterest));
    }

    public void Clear(View v)
    {
        loanText.setText("");
        yearsText.setText("");
        interestText.setText("");
        monthlyPaymentText.setText("");
        totalPaymentText.setText("");
        totalInterestText.setText("");
    }

    private double round(Double number)
    {
        return Math.round(number * 100) / 100.;
    }

    private void wrongCredentials()
    {
        loanText.setText("");
        yearsText.setText("");
        interestText.setText("");
        monthlyPaymentText.setText("NAN");
        totalPaymentText.setText("NAN");
        totalInterestText.setText("NAN");
    }
}
